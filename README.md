### How to run the app with docker (recommended)

First you need to install Docker and docker compose. Instructions can be found [here](https://docs.docker.com/install/) and [here](https://docs.docker.com/compose/install/).
Assumed that you got installed Docker (or after installation), open a terminal and:

1. Clone the repo `git clone https://bitbucket.org/anarchos78/super-carers.git`.
2. Go into the application directory `cd super-carers`.
3. To build and run the container issue the command `docker-compose up --build -d`.

The latter command, will build the images, run the application's unit tests and bring up the containers.
In order to serve the weather application we use `gunicorn` to handle the requests for the flask app and `nginx` to proxy connections to Gunicorn which brings certain performance benefits.

If you want to stop the container issue `docker-compose down`.

### Calls

Open a browser and paste in the address bar:
```sh
http://127.0.0.1:8080/weather/v1/london/20180425/1231/
```

You'll see forecast info for the day `2018-04-25` and time `12:31`.
You can replace the date and time parts of the above URL. Keep in mind that date and time parts must not have delimiters ("-" for date and ":" for time). The result is in JSON format.

```json
{
    "description": "scattered clouds",
    "temperature": "14C",
    "pressure": 1020.34,
    "humidity": "62%"
}
```

You can pass a querystring like:
```sh
http://127.0.0.1:8080/weather/v1/london/20180425/1231/?temp_unit=k
```

It will return forecast info for the respective date and time but the temperature will be shown in Kelvin.
The URL parameter `temp_unit` can take the arguments of `c` or `k`. The defaulte temperature unit is Celsius.

```json
{
    "description": "scattered clouds",
    "temperature": "286.695K",
    "pressure": 1020.34,
    "humidity": "62%"
}
```

Also you can ask for individual parts of the forcast info. The available parts are `description`, `temperature`, `pressure` and `humidity`.
For example to get the general forecast description of a specific day and time, in your browser's address bar paste:

```sh
http://127.0.0.1:8080/weather/v1/london/20180425/1231/description/
```
that will return
```json
{
    "description": "scattered clouds"
}
```
or
```sh
http://127.0.0.1:8080/weather/v1/london/20180425/1231/pressure/
```
that will return
```json
{
    "pressure": 1020.34
}
```
The `temperature` endpoint can take the parameter `temp_unit` to ask the temperature either in Celsius or in Kelvin.
The URL parameter `temp_unit` can take the arguments of `c` or `k`. The defaulte temperature unit is Celsius.
Example:
```sh
http://127.0.0.1:8080/weather/v1/london/20180425/1231/temperature/?temp_unit=k
```
that will return
```json
{
    "temperature": "286.695K"
}
```

### How to run the app locally (for development)
Open a terminal and:

1. Clone the repo `git clone https://bitbucket.org/anarchos78/super-carers.git`.
2. Go into the application directory `cd super-carers/weather_app`.
3. Create a virtual environment for the project, using python 3.6.x and activate it (I prefer ***pyenv***).
4. While in the activated virtual environment run `pip install -r requirements.txt` to install the required packages.
5. To run the tests in the project's directory issue the command `nosetests` or `python -W ignore:ResourceWarning -m unittest discover`.
6. To run the coverage tests issue the command `coverage run weather/utils.py && coverage html`. That will produce a directory `htmlcov`.
There, you can find the coverage test results.
7. Run the server: `python run.py`.

Then you can make the same calls as above, but you have to change the post from `8080` to `5000`.


### Remarks

- I have chosen the above stack (Gunicorn, Nginx) to make the API production ready.

- You may notice that the API endpoints (file `weather_app.weather.api`) are lean.
The reason is that the API implementation-wise was simple, as such the workhorse of all endpoints is the function `get_forecast` in `weather_app.weather.utils`.

- In the `weather_app.config.config.py` file, in the `BaseConfig` class, there is an attribute named `OPEN_WEATHER_MAP_API_KEY`.
This attribute holds the http://api.openweathermap.org API key. As you can see, the API key is expected to be found in the environment 
(this is the best way to set sensitive values in my opinion). For the sake of the task, I set a default value (my API key).
This is not the optimal way! Submitting such info to a repo is not a good idea.

- We should always version APIs to accommodate backwards compatibility in the future, hence the URLs includes the API version `http://127.0.0.1:8080/weather/v1`.


### Answers to your questions:

 - If I wanted the temperature in Kelvin rather than celsius, how could I specify this in API calls?
 
    > I already implemented that by using a URL parameter called `temp_unit`

 - How would you test this REST service?
 
    > By using unit tests (already wrote) pytest etc. It depends on the development/production process and the application itself.
      I would use a CI approach and augment tests with integration tests if the API was a part of a larger application.

 - How would you check the code coverage of your tests?
 
    > By using python package `coverage` (already used it).
      Unfortunately, the code coverage is not good and it does not cover the whole app.
      Sometimes it's not possible to cover all the code paths (I'm not saying that it's not doable to this case).

 - How could the API be documented for third-parties to use?
 
    > I did't implement it. I would use `Swagger` or `Sphinx`.

 - How would you restrict access to this API?
 
    > It's not related to API access restriction but it's related to API security: I implemented rate limiting.
      The reasoning for rate limit implementation is to allow a better flow of data, and increased security by mitigating attacks such as DDoS.      
      I would use API keys if users don’t need to access more than a single user’s data (like http://api.openweathermap.org)
      or JWT (Json Web Token). There is a flask JWT extension [here](https://github.com/vimalloc/flask-jwt-extended).
      Of course always use SSL.

 - What would you suggest is needed to do daily forecast recoveries from openweather.org, keeping the web service up to date?
 
    > In order to keep the service up to date I would use a cron job that would make API calls to http://api.openweathermap.org (every 3 hours). The architectural approach for keeping the service up to date would include 3 separate services/containers:
     
     > 1. Database container: the container will be responsible to hold the harvested forecast data from http://api.openweathermap.org
     
     > 2. Data collection container: the forecast collection service will run as a scheduled job (cron).
           It will get data from http://api.openweathermap.org every 3 hours (5 day / 3 hour forecast data according to https://openweathermap.org/forecast5)
           The service will connect to the `Database container` and will store the fetched data.
           It will update the already stored to the database data if the freshly fetched data overlaps with the existing.
           
	> 3. The API service container: the forecast data served by the service will come from the `Database container`.      
      The only container that would be publicly accessible is the `API service container`.

**Enjoy!**
