# -*- coding: utf-8 -*-

"""
.. module:: .utils.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import math
import datetime

import requests

from flask import current_app


def fetch_weather_data():
    """
    Fetches forecast data from www.openweathermap.org
    :return: obj
    """

    url = "http://api.openweathermap.org/data/2.5/forecast"

    headers = {
        "user-agent": (
            "Mozilla/5.0 "
            "(Macintosh; Intel Mac OS X 10_13_4) "
            "AppleWebKit/537.36 (KHTML, like Gecko) "
            "Chrome/65.0.3325.181 Safari/537.36"
        )
    }

    payload = {
        "q": "London,uk",
        "APPID": current_app.config["OPEN_WEATHER_MAP_API_KEY"]
    }

    response = None
    try:
        response = requests.get(url, headers=headers, params=payload)
        response.raise_for_status()
    except requests.exceptions.RequestException as err:
        current_app.logger.error(
            f"An exception occurred, {err}"
        )

    return response.json()


def get_forecast(date, time, **kwargs):
    """
    Parses the response from www.openweathermap.org.
    Returns filtered results based on the date and time passed.
    Additionally, returns individual parts of the forecast such as description,
    temperature, pressure and humidity.
    Lastly, when `temp_unit` argument is passed, the temperature value
    is returned in Celsius or Kelvin depending on
    the parameter value (options: 'c', ';').
    """

    # Filter the list
    dt_txt_list = [
        dt_txt
        for dt_txt
        in fetch_weather_data().get("list")
        if convert_dt_txt(dt_txt.get("dt_txt"))[0] == date
        and len(time) == 4
        and (
            int(convert_dt_txt(dt_txt.get("dt_txt"))[1])
            <= int(time)
            < int(convert_dt_txt(dt_txt.get("dt_txt"))[1]) + 300
        )
    ]

    if dt_txt_list:
        main = dt_txt_list[0].get('main')
        weather = dt_txt_list[0].get('weather')[0]

        forecast = {
            "description": weather['description'],
            "temperature": f"{convert_kelvin_to_celsius(main['temp'])}C",
            "pressure": main['pressure'],
            "humidity": f"{main['humidity']}%"
        }

        if 'temp_unit' not in kwargs:
            kwarg = kwargs.popitem()[0]
            if kwarg in forecast:
                return {kwarg: forecast[kwarg]}
        else:
            temp_unit = kwargs['temp_unit'].lower()

            if len(kwargs) > 1:
                key = next(iter(forecast.keys() & kwargs.keys()))

                if temp_unit == 'k':
                    forecast['temperature'] = f"{main['temp']}K"

                return {key: forecast[key]}
            else:
                if temp_unit == 'k':
                    forecast['temperature'] = f"{main['temp']}K"

                return forecast
    else:
        current_app.logger.warning(
            f"No data for {format_date(date)} {format_time(time)}"
        )
        return {
            "status": "error",
            "message": (
                f"No data for {format_date(date)} {format_time(time)}"
            )
        }


def convert_dt_txt(dt_txt):
    """
    Converts a string like `2018-04-21 12:00:00`
    to a tuple of ('2018421', '1200')
    """

    date = datetime.datetime.strptime(dt_txt, "%Y-%m-%d  %H:%M:%S")

    return (
        f"{date.year}{date.month:02d}{date.day:02d}",
        f"{date.hour:02d}{date.minute:02d}"
    )


def convert_kelvin_to_celsius(temp):
    """
    Converts kelvin to celsius rounded up
    """

    return math.ceil(temp - 273.15)


def format_date(date):
    """
    Converts a string like `20180421` to a string `2018-04-21`
    """

    # Check string length. We expect a string length of 8.
    # If string diverges from that just return it
    if len(date) == 8:
        return f"{date[:4]}-{date[4:6]}-{date[6:]}"
    else:
        return f"{date} (date not valid)"


def format_time(time):
    """
    Converts a string like `2100` to a string `21:00`
    """

    # Check string length. We expect a string length of 4.
    # If string diverges from that just return it
    if len(time) == 4 and 0 <= int(time) < 2400:
        return f"{time[:2]}:{time[2:]}"
    else:
        return f"{time} (time not valid)"


if __name__ == "__main__":
    convert_dt_txt('2018-04-21 12:00:00')

    convert_kelvin_to_celsius(300)

    format_date('20180421')
    format_date('201804210')

    format_time('2100')
    format_time('210')
