# -*- coding: utf-8 -*-

"""
.. module:: .config.py
   :synopsis: <write a synopsis>

.. moduleauthor:: Athanasios Rigas
                  <admin@ithemis.gr>
"""

import os
import logging

from logging.handlers import RotatingFileHandler

from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

from weather.api import weather


class BaseConfig(object):
    """
    Base configuration class.
    Here you can define common configuration settings
    that other config classes will inherit from
    """
    DEBUG = False
    TESTING = False

    LOGGING_DIR = "logs"
    os.makedirs(LOGGING_DIR, exist_ok=True)

    LOGGING_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    LOGGING_APP_LOCATION = f"{LOGGING_DIR}/service.log"
    LOGGING_RT_LOCATION = f"{LOGGING_DIR}/rate-limit.log"
    LOGGING_LEVEL_INFO = logging.INFO
    LOGGING_LEVEL_DEBUG = logging.DEBUG

    OPEN_WEATHER_MAP_API_KEY = os.getenv(
        "OPEN_WEATHER_MAP_API_KEY", "5ece29ebab6810375cf4bc4936811526"
    )

    RATE_LIMIT = False


class DevConfig(BaseConfig):
    """
    Development configuration
    """
    DEBUG = True
    TESTING = True


class TestConfig(BaseConfig):
    """
    Test configuration
    """
    DEBUG = False
    TESTING = True


class ProdConfig(BaseConfig):
    """
    Test Production
    """
    DEBUG = False
    TESTING = False
    RATE_LIMIT = True


def configure_app(app):
    """
    App configuration
    """
    app.config.from_object('config.config.ProdConfig')

    app.register_blueprint(weather)

    # Configure app logging
    handler = RotatingFileHandler(
        app.config['LOGGING_APP_LOCATION'], maxBytes=1000000, backupCount=5
    )
    formatter = logging.Formatter(app.config['LOGGING_FORMAT'])
    handler.setFormatter(formatter)
    app.logger.addHandler(handler)
    app.logger.setLevel(app.config['LOGGING_LEVEL_INFO'])

    # Set rate limit per blueprint.
    # Ensure that no one accidentally or
    # deliberately affect the API availability
    if app.config['RATE_LIMIT']:
        limiter = Limiter(
            app,
            key_func=get_remote_address,
            default_limits=["60/minute", "60/day"]
        )
        limiter.limit(
            limit_value="60/minute;180/day",
            error_message=(
                "You have exceeded your API calls limit. "
                "You should wait a while before using the API again."
            )
        )(weather)

        # Configure rate limiter logging
        rate_limiter_handler = RotatingFileHandler(
            app.config['LOGGING_RT_LOCATION'], maxBytes=1000000, backupCount=5
        )
        rate_limiter_handler.setFormatter(formatter)
        limiter.logger.addHandler(rate_limiter_handler)
        rate_limiter_handler.setLevel(app.config['LOGGING_LEVEL_DEBUG'])
